import {FETCH_SUNSET_SUNRISE_INFO_SUCCESS} from "../../common/action_constants";

const sunsetSunrise = (state = {}, action) => {
    switch (action.type) {
        case FETCH_SUNSET_SUNRISE_INFO_SUCCESS: {
            return action.sunsetSunrise;
        }

        default :
            return state;
    }
};

export default sunsetSunrise;