import {createStore, applyMiddleware} from 'redux';
import {composeWithDevTools} from 'redux-devtools-extension/developmentOnly';
import thunk from 'redux-thunk';
import createRootReducer from '../reducers';
import {routerMiddleware} from 'connected-react-router'
import history from '../history';

let initialState = {};

const routerHistoryMiddleware = routerMiddleware(history);
const middleware = [thunk, routerHistoryMiddleware];

const composeEnhancers = composeWithDevTools({
    // Specify here name, actionsBlacklist, actionsCreators and other options
});

const store = createStore(createRootReducer(history), initialState, composeEnhancers(
    applyMiddleware(...middleware)
));


export default store;
