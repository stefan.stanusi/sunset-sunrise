import axios from 'axios';


// ENVIRONMENT VARIABLES
export const BACKEND_URL = 'https://api.sunrise-sunset.org/json';

export function callAPI(dispatch, method = 'GET', path = '', data, token = '', language = 'en', additionalHeadders, responseType) {
    let request = {
        method: method,
        url: `${BACKEND_URL}${path}`,
        credentials: 'same-origin',
    };
    if (!!responseType) {
        request = {...request, responseType}
    }
    if (data) {
        request['data'] = data;
    }
    let headers = {};
    if (token) {
        headers['Authorization'] = 'Bearer ' + token;
    }
    if (typeof language !== 'undefined' && language !== null) {
        headers['Accept-Language'] = Array.isArray(language) ? language.join('|').toLowerCase().split('|') : language.toLowerCase();
    }
    if (additionalHeadders) {
        headers = {
            ...headers,
            ...additionalHeadders
        };
    }
    request['headers'] = headers;
    return axios(request).then((response) => {
        return Promise.resolve(response);
    }).catch((error) => {
        if (error.response) {
            return Promise.reject(error);
        }
    });
}

export function fetchJson(dispatch, path, token, language, responseType) {
    return callAPI(dispatch, 'GET', path, null, token, language, {
            'Content-Type': 'application/json'
        },
        responseType
    );
}