import React from 'react';
import ReactMapGL, {
    FlyToInterpolator,
    FullscreenControl,
    GeolocateControl,
    Marker,
    NavigationControl,
    Popup,
    ScaleControl
} from "react-map-gl";
import PopupContent from "./PopupContent";
import {connect} from "react-redux";
import {fetchSunsetSunriseInfo} from "../redux/actions/sunsetSunriseActions";
import {getIsFetchingSunsetSunrise, getSunsetSunrise} from "../redux/reducers";

const ICON = `M20.2,15.7L20.2,15.7c1.1-1.6,1.8-3.6,1.8-5.7c0-5.6-4.5-10-10-10S2,4.5,2,10c0,2,0.6,3.9,1.6,5.4c0,0.1,0.1,0.2,0.2,0.3
  c0,0,0.1,0.1,0.1,0.2c0.2,0.3,0.4,0.6,0.7,0.9c2.6,3.1,7.4,7.6,7.4,7.6s4.8-4.5,7.4-7.5c0.2-0.3,0.5-0.6,0.7-0.9
  C20.1,15.8,20.2,15.8,20.2,15.7z`;

class Map extends React.PureComponent {
    constructor(props) {
        super(props);
        this.setUserLocation = this.setUserLocation.bind(this);
        this.setViewPort = this.setViewPort.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.state = {
            accessToken: "pk.eyJ1Ijoic3RhbnVzaXN0ZWZhbmNvc21pbiIsImEiOiJjazZkMnRscXowMm91M2xtcHdwY3M3Mm10In0.WN4-zLUK-HSwRdLfdTBvSw",
            viewport: {
                latitude: 0,
                longitude: 0,
                zoom: 2,
            },
            userLocation: {
                latitude: 0,
                longitude: 0,
                zoom: 2,
            },
            selectedLocation: {
                latitude: 0,
                longitude: 0,
                zoom: 2
            },
            initialSelect: false,

        }
    }

    setViewPort(viewport) {
        this.setState({viewport});
    }

    handleClick(e) {
        if (e.target.className === 'mapboxgl-ctrl-icon') return;

        const {lngLat: [longitude, latitude]} = e;
        this.props.fetchSunsetSunriseInfo(latitude, longitude);
        this.setState((prevState) => ({
            selectedLocation: {
                latitude,
                longitude,
                zoom: 2
            },
            viewport: {
                latitude,
                longitude,
                zoom: prevState.viewport.zoom,
                transitionInterpolator: new FlyToInterpolator({speed: 2}),
                transitionDuration: 'auto'
            }
        }), () => {
            if (!this.state.initialSelect) this.setState({initialSelect: true,})
        })
    }

    setUserLocation() {
        navigator.geolocation.getCurrentPosition(position => {
            let newViewport = {
                height: "100vh",
                width: "100vw",
                latitude: position.coords.latitude,
                longitude: position.coords.longitude,
                zoom: 2,
                transitionInterpolator: new FlyToInterpolator({speed: 2}),
                transitionDuration: 'auto'
            };
            this.setState({
                userLocation: newViewport,
                viewport: newViewport,
            })
        }, (error) => {
            console.log(error);
        })
    }

    componentDidMount() {
        this.setUserLocation();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.sunsetSunrise !== prevProps.sunsetSunrise) {
            const date = new Date();
            const hours = date.getUTCHours();
            const minutes = date.getUTCMinutes();
            const seconds = date.getUTCSeconds();
            const time = 3600 * hours + 60 * minutes + seconds;
            this.setState({
                daytime: (time >= this.props.sunsetSunrise.sunrise && time <= this.props.sunsetSunrise.sunset)
            });
        }
    }

    render() {
        const {isFetchingSunsetSunrise} = this.props;
        const {viewport, userLocation, accessToken, selectedLocation, initialSelect, daytime} = this.state;
        return (
            <ReactMapGL {...viewport}
                        mapboxApiAccessToken={accessToken}
                        width="100vw"
                        height="100vh"
                        mapStyle="mapbox://styles/mapbox/dark-v9"
                        onViewportChange={viewport => this.setViewPort(viewport)}
                        onClick={this.handleClick}

            >

                <div className={'geolocation'}>
                    <GeolocateControl
                        showUserLocation={false}
                        positionOptions={{enableHighAccuracy: true}}
                        trackUserLocation={true}
                        onClick={(e) => {
                            e.preventDefault();
                            e.stopPropagation();
                            this.setUserLocation();
                        }}
                    />
                </div>

                <div className={'fullscreen'}>
                    <FullscreenControl/>
                </div>
                <div className={'nav'}>
                    <NavigationControl/>
                </div>
                <div className={'scale'}>
                    <ScaleControl/>
                </div>

                <Marker className={'marker'}
                        key={`current-location-marker`}
                        longitude={userLocation.longitude}
                        latitude={userLocation.latitude}>
                    <svg
                        height={20}
                        viewBox="0 0 24 24"
                        className={'marker__svg marker__svg--current'}
                    >
                        <path d={ICON}/>
                    </svg>
                </Marker>

                {initialSelect && <React.Fragment>
                    <Marker className={'marker'}
                            key={`selected-location-marker`}
                            longitude={selectedLocation.longitude}
                            latitude={selectedLocation.latitude}>
                        <svg
                            height={20}
                            viewBox="0 0 24 24"
                            className={'marker__svg'}
                        >
                            <path d={ICON}/>
                        </svg>
                    </Marker>

                    <Popup
                        tipSize={5}
                        anchor="top"
                        longitude={selectedLocation.longitude}
                        latitude={selectedLocation.latitude}
                    >
                        <PopupContent isFetchingSunsetSunrise={isFetchingSunsetSunrise} daytime={daytime}/>
                    </Popup>
                </React.Fragment>
                }

            </ReactMapGL>
        );
    }
}

function mapStateToProps(state) {
    return {
        sunsetSunrise: getSunsetSunrise(state),
        isFetchingSunsetSunrise: getIsFetchingSunsetSunrise(state),
    };
}

export default connect(mapStateToProps, {fetchSunsetSunriseInfo})(Map);