import React from 'react';

class ErrorBoundary extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            hasError: false,
            error: null
        };
    }

    static getDerivedStateFromError() {
        // Update state so the next render will show the fallback UI.
        return {hasError: true};
    }

    componentDidCatch(error, info) {
        const trace = {
            componentStack: info.componentStack.split('\n').slice(1).join('\n'),
            stack: error.stack
        };
        const errorLog = {
            'description': String(error).split(': ')[1],
            'name': String(error).split(':')[0],
            'trace': JSON.stringify(trace),
        };
        this.setState({error: errorLog})
    }

    render() {
        if (this.state.hasError) {
            return (
                <div className="l-main-content">
                    <p className="block error-message-block">
                        {this.state.error}
                    </p>
                </div>
            );
        }

        return this.props.children;
    }
}


export default (ErrorBoundary);
