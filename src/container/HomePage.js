import React from 'react';
import Map from "../components/Map";

function HomePage() {
    return (
        <div className={'l-content homepage'}>
            <Map/>
        </div>
    );
}

export default React.memo(HomePage);