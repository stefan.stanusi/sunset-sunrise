import {createBrowserHistory} from 'history';

const history = typeof window !== 'undefined' ? createBrowserHistory() : undefined;

export default history;