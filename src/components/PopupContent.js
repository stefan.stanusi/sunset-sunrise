import React from 'react';
import Sun from '../img/sun.png'
import Moon from '../img/moon.png'
import Loader from "./Loader";

function PopupContent({daytime, isFetchingSunsetSunrise}) {
    return (
        <Loader show={isFetchingSunsetSunrise}>
            <div>
                {!isFetchingSunsetSunrise && <img src={daytime ? Sun : Moon} width={64} height={64} alt={''} className={daytime ? 'mt--6' : ''}/>}
            </div>
        </Loader>
    );
}

export default (PopupContent);