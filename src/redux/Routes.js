import React from 'react';
import {Route, Switch} from 'react-router-dom';
import {ConnectedRouter as Router} from 'connected-react-router'
import history from './history';
import HomePage from "../container/HomePage";
import {Redirect} from "react-router";
import Layout from "../components/Layout";
import ErrorBoundary from "../components/ErrorBoundary";


function Routes(props) {
    return (
        <Router history={history}>
            <ErrorBoundary props={props}>
                <Layout props={props}>
                    <Switch>
                        <Route exact path="/" component={HomePage}/>
                        <Redirect to="/" component={HomePage}/>
                    </Switch>
                </Layout>
            </ErrorBoundary>
        </Router>
    );
}

export default React.memo(Routes);
