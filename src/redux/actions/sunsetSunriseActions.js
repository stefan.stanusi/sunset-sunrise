import {fetchJson} from "../../rest/Interface";
import {
    FETCH_SUNSET_SUNRISE_INFO_FAILURE,
    FETCH_SUNSET_SUNRISE_INFO_REQUEST,
    FETCH_SUNSET_SUNRISE_INFO_SUCCESS
} from '../../common/action_constants';

export function fetchSunsetSunriseInfo(lat, lng) {
    function processTime(time) {
        const AMPM = time.split(" ")[1];
        let fullTime = time.split(" ")[0];
        let hours = fullTime.split(":")[0];
        const minutes = fullTime.split(":")[1];
        const seconds = fullTime.split(":")[2];
        if (AMPM === "PM" && parseInt(hours) < 12) hours = parseInt(hours) + 12;
        if (AMPM === "AM" && hours === 12) hours = parseInt(hours) - 12;
        return 3600 * hours + 60 * minutes + parseInt(seconds);
    }

    return (dispatch, getState) => {
        dispatch({
            type: FETCH_SUNSET_SUNRISE_INFO_REQUEST
        });
        fetchJson(dispatch, '?lat=' + lat + '&lng=' + lng)
            .then((response) => {
                if (response && response.status === 200 && !!response.data) {
                    let {sunrise, sunset} = response.data.results;
                    dispatch({
                        type: FETCH_SUNSET_SUNRISE_INFO_SUCCESS,
                        sunsetSunrise: {
                            sunrise: processTime(sunrise),
                            sunset: processTime(sunset)
                        }
                    })
                } else {
                    dispatch({
                        type: FETCH_SUNSET_SUNRISE_INFO_FAILURE
                    });
                }
            }, (error) => {
                dispatch({
                    type: FETCH_SUNSET_SUNRISE_INFO_FAILURE
                });

            });
    }
}