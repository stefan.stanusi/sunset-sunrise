import React from 'react';
import {withRouter} from "react-router";
import NotificationContainer from "react-notifications/lib/NotificationContainer";

function Layout({children, ...props}) {
    return (
        <div className={'l-app'}>
            <NotificationContainer/>
            <div
                className={'l-page homepage'}>
                <div className="l-main">
                    {children}
                </div>
            </div>
        </div>
    );
}

export default withRouter(React.memo(Layout));
