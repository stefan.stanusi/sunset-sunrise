import React from 'react';
import PropTypes from 'prop-types';
import FoldingCube from "better-react-spinkit/dist/FoldingCube";

/**
 * Loader used to wrap components that require data fetching to render
 * @example
 * <Loader show={this.state.isLoading}>
 *     {children}
 * </Loader>
 */
class Loader extends React.PureComponent {

    render() {
        const {
            show,
            opaque,
            children,
            className,
            size
        } = this.props;
        return (
            <div className={'loader' + (opaque ? ' opaque' : '') + (className ? ' ' + className : '')}>
                {children}
                {show ? (
                    <div className="loader-overlay">
                        <FoldingCube timingFunction={'ease'} name="folding-cube" size={size || 40} color="#53A1F0"/>
                    </div>
                ) : null}
            </div>
        );
    }
}

Loader.propTypes = {
    show: PropTypes.bool,
    opaque: PropTypes.bool,
    children: PropTypes.node,
    loadingContent: PropTypes.node
};

Loader.defaultProps = {
    show: true,
    opaque: false,
};

export default Loader;
