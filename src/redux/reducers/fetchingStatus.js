import {
    FETCH_SUNSET_SUNRISE_INFO_FAILURE,
    FETCH_SUNSET_SUNRISE_INFO_REQUEST,
    FETCH_SUNSET_SUNRISE_INFO_SUCCESS
} from "../../common/action_constants";
import {combineReducers} from "redux";

const fetchingStatus = () => {

    const isFetchingSunsetSunrise = (state = false, action) => {
        switch (action.type) {
            case  FETCH_SUNSET_SUNRISE_INFO_REQUEST:
                return true;
            case  FETCH_SUNSET_SUNRISE_INFO_SUCCESS:
            case  FETCH_SUNSET_SUNRISE_INFO_FAILURE:
                return false;
            default:
                return state;
        }
    };
    return combineReducers({
        isFetchingSunsetSunrise,
    });
};


export default fetchingStatus();

export const getIsFetchingSunsetSunrise = (state) => state.isFetchingSunsetSunrise;
