import {connectRouter} from "connected-react-router";
import {combineReducers} from "redux";
import sunsetSunrise from "./sunsetSunrise";
import fetchingStatus, * as statusSelectors from "./fetchingStatus";

export default (history) => combineReducers({
    router: connectRouter(history),
    sunsetSunrise,
    fetchingStatus,
});

export const getSunsetSunrise = (state) => state.sunsetSunrise;

export const getIsFetchingSunsetSunrise = (state) => statusSelectors.getIsFetchingSunsetSunrise(state.fetchingStatus);

