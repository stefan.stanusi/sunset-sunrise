import React from 'react';
import Routes from './redux/Routes';
import {Provider} from "react-redux";
import store from "./redux/store/configureStore";


function Root() {
    return (
        <Provider store={store}>
            <Routes/>
        </Provider>
    );

}

export default React.memo(Root);
